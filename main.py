import webapp2

from google.appengine.ext import ndb

class Ip_addr(ndb.Model):
	ip_addr = ndb.StringProperty(indexed=True)
	email = ndb.StringProperty(indexed=True)
	pwd = ndb.StringProperty(indexed=True)
	
class MainPage(webapp2.RequestHandler):
	def get(self):
		self.response.headers['Content-Type'] = 'text/plain'
		self.response.write('Hello, World !')

		
class Desktop(webapp2.RequestHandler):
	def post(self):
		mail = str(self.request.get('email'))
		string_query = "SELECT * FROM Ip_addr WHERE email = '" + mail+ "'"
		ip = ndb.gql(string_query).get()
		if ip is None:
			user = Ip_addr(email = self.request.get('email'), ip_addr = self.request.get('ip_addr'), pwd = self.request.get('pwd'))
			user.put()
		else:
			ip.ip_addr = self.request.get('ip_addr')
		self.response.write('Submitted')
		
	def get(self):
		self.response.headers['Content-Type'] = 'text/plain'
		self.response.write('Hello, Desktop !')
		
		
class Mobile(webapp2.RequestHandler):
	def post(self):
		mail = str(self.request.get('email'))
		pwd = str(self.request.get('pwd'))
		string_query = "SELECT ip_addr FROM Ip_addr WHERE email = '" + mail+ "' AND pwd = '" + pwd + "'"
		ip = ndb.gql(string_query).get()
		if ip is None:
			self.response.write('NONE')
		else:
			self.response.write(ip.ip_addr)
		
	def get(self):
		self.response.headers['Content-Type'] = 'text/plain'
		self.response.write('Hello, Mobile !')
	
	
app = webapp2.WSGIApplication([
	('/', MainPage),
	('/mobile', Mobile),
	('/desktop', Desktop)
], debug=True)